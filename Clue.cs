﻿using System;

namespace coderace
{
    public class Clue
    {
        //Auto-implemented properties
        public int id { get; set; }
        public int game_id { get; set; }
        public string type { get; set; }
        public int latitude { get; set; }
        public int longitude { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string clue { get; set; }
        public Boolean visible { get; set; }
        public int player_id { get; set; }
        public Boolean claimed { get; set; }

        //Construtor - will be invoked by JSON deserialiser
        public Clue()
        {
        }
    }

}