<?php
        include("config.php");
        session_start();
        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
                $player_id = $_POST["player_id"];
                $team_id = $_POST["team_id"];
                $send = $_POST["send"];
                $message = $_POST["message"];
                $time = $_POST["time"];
                
                if($send == "0")
                {
                        //We're retrieving messages, and passing them back to the app as JSON strings
                        $retrieve = $db->query("SELECT message.message, player.username FROM message JOIN team ON message.team_id = team.id JOIN player ON message.player_id = player.id WHERE team.id = $team_id ORDER BY message.received" );
                        $numrows = mysqli_num_rows($retrieve);
                        $i = 1;
                        echo "[";
                        while ($row = mysqli_fetch_assoc($retrieve))
                        {
                                echo json_encode($row);
                                if($i < $numrows)
                                {
                                        echo ",";
                                }
                                $i++;
                        }
                        echo "]";
                }
                elseif($send == "1")
                {
                        //Need the max id in order to insert the NEXT one
                        $maxidquery = mysqli_query($db, "SELECT MAX(id) as max FROM message");
                        $result = mysqli_fetch_object($maxidquery);
                        $maxid = $result->max;
                        $maxid++;
                
                        $db->query("INSERT INTO message (id, team_id, player_id, message, received) VALUES ($maxid, $team_id, $player_id, '$message', $time)");
                }
        }
        elseif($_SERVER["REQUEST_METHOD"] == "GET")
        {
                $player_id = "1";
                $team_id = 1;
                $send = "0";
                $message = "none";
                $time = "none";
                
                if($send == "0")
                {
                        //We're retrieving messages, and passing them back to the app as JSON strings
                        $retrieve = $db->query("SELECT message.message, player.username FROM message JOIN team ON message.team_id = team.id JOIN player ON message.player_id = player.id WHERE team.id = 1 ORDER BY message.received" );
                        $numrows = mysqli_num_rows($retrieve);
                        $i = 1;
                        echo "[";
                        while ($row = mysqli_fetch_assoc($retrieve))
                        {
                                echo json_encode($row);
                                if($i < $numrows)
                                {
                                        echo ",";
                                }
                                $i++;
                        }
                        echo "]";
                }
        }
?>
