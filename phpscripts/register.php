<?php
        include("config.php");
        session_start();
        
        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
                $username = $_POST["username"];
                $hashedpw = sha1($_POST["password"]);
                $team_id = $_POST["team_id"];
                
                //Need the max id in order to insert the NEXT one
                $maxidquery = mysqli_query($db, "SELECT MAX(id) as max FROM player");
                $result = mysqli_fetch_object($maxidquery);
                $maxid = $result->max;
                $maxid++;
                
                $adduser = mysqli_query($db, "INSERT INTO player (id, username, password, team_id) VALUES ($maxid, '$username', '$hashedpw', $team_id)");
                return "Registered. $username can now be logged into.";
        }
        elseif($_SERVER["REQUEST_METHOD"] == "GET")
        {
                $i = 1;
                $team_query = mysqli_query($db, "SELECT name, id FROM team");
                $numrows = mysqli_num_rows($team_query);
                
                while ($row = mysqli_fetch_assoc($team_query))
                {
                        echo $row["id"] . "-" . $row["name"];
                        if($i < $numrows)
                        {
                                echo ",";
                                $i++;
                        }
                }
        }
?>
