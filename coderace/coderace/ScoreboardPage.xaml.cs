﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScoreboardPage : ContentPage
	{
        public bool finish;

        public ScoreboardPage(UserSession session, bool finishedgame)
        {
            InitializeComponent();

            finish = finishedgame;

            using (WebClient client = new WebClient())
            {

                NameValueCollection postData = new NameValueCollection
                {
                    ["player_id"] = session.id
                };

                var response = client.UploadValues("http://coderace.napier.ac.uk/scoreboard.php", postData);
                if (Encoding.Default.GetString(response).Equals("0:"))
                {
                    //Need to handle players having no points, as this will crash the app otherwise
                    PlayerScore.Text = "Your individual score: 0";
                    PlayerTeam.Text = "No teams have earned points!";

                }
                string[] split = Encoding.Default.GetString(response).Split(':');
                PlayerScore.Text = "Your individual score: " + split[0];
                PlayerTeam.Text = "You are on team: " + split[1];

                var score = client.DownloadString("http://coderace.napier.ac.uk/totalscores.php");

                string[] rows = score.Split(',');

                if (finish)
                {
                    string[] part = rows[0].Split(':');
                    Label finished = new Label
                    {
                        Text = "Game over! The winner is: " + part[0] + "!",
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                        VerticalOptions = LayoutOptions.Start,
                        HorizontalOptions = LayoutOptions.Center
                    };
                }

                foreach (string row in rows)
                {
                    string[] part = row.Split(':');

                    Label label = new Label
                    {
                        Text = part[0] + "        " + part[2],
                        TextColor = Color.FromHex(part[1]),
                        VerticalOptions = LayoutOptions.Start,
                        HorizontalOptions = LayoutOptions.Center
                    };

                    stack.Children.Add(label);
                }

            }

            if (session.username.Equals("admin"))
            {
                //Add a start game button and end game button
                //If the game is ended/not started then taking locations will be locked.
                //When the game ends, the scorebard page will display winners and other assorted things.

                var startgame = new Button
                {
                    Text = "Start game",
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center
                };
                startgame.Clicked += async (s, e) =>
                {
                    //TODO - handle game start. Also ensure users can only take locations if a game has started.
                };

                var endgame = new Button
                {
                    Text = "End game",
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center
                };
                endgame.Clicked += async (s, e) =>
                {
                    //TODO - handle game end. Also ensure users can't take locations when a game ends. Also change scoreboard based on game ending.
                };
            }
        }

        protected override bool OnBackButtonPressed()
        {
            if(finish)
            {
                Navigation.PushModalAsync(new NavigationPage(new LogIn()));
                return true;
            }
            else
            {
                return base.OnBackButtonPressed();
            }
        }
    }
}