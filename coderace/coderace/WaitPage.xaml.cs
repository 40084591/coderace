﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WaitPage : ContentPage
	{
        WebClient client = new WebClient();
        public WaitPage (UserSession session)
		{
            InitializeComponent();

            var worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerAsync();

        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                //Check if a game is running  every minute. If one is, pop the navigation stack to go back to the map screen.

                string isgame = client.DownloadString("http://coderace.napier.ac.uk/checkgame.php");
                if (!isgame.Equals("0"))
                {
                    Navigation.PopModalAsync();
                    break;
                }
                Thread.Sleep(30000);
            }
        }
    }
}