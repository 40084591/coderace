﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{

        public RegisterPage ()
		{
			InitializeComponent ();
            //TODO - PHP script needs written
            //team.Items.Add("Testers");
            //team.Items.Add("Opponents");
            //try
            //{
                WebClient client = new WebClient();
                var response = client.DownloadString("http://coderace.napier.ac.uk/register.php/");

                string[] names = response.Split(',');

                foreach (string name in names)
                {
                    team.Items.Add(name);
                }
            //}
            //catch (Exception ex)
            //{
            //    DisplayAlert("Warning", "An internet connection is required to play CodeRace.", "OK");
            //}

        }




        async void CreateButtonClicked(object sender, EventArgs e)
        {
            Console.WriteLine("Create button has been clicked");
            int team_id = 0;
            if (team.SelectedIndex == -1)
            {
                await DisplayAlert("Can't create user", "Please select a team", "OK");
            }
            else
            {

                //Take the ID of the selected team
                string[] id = team.SelectedItem.ToString().Split('-');
                team_id = int.Parse(id[0]);


                using (WebClient client = new WebClient())
                {
                    NameValueCollection postData = new NameValueCollection
                    {
                        ["username"] = username.Text,
                        ["password"] = password.Text,
                        ["team_id"] = team_id.ToString()
                    };

                    //try
                    //{ 
                        var register = client.UploadValues("http://coderace.napier.ac.uk/register.php", postData);
                        await DisplayAlert("Registered", Encoding.Default.GetString(register), "OK");

                        var response = client.UploadValues("http://coderace.napier.ac.uk/login.php", postData);

                        if (Encoding.Default.GetString(response).Equals("Unsuccessful login"))
                        {
                            await DisplayAlert("Failure", Encoding.Default.GetString(response), "OK");
                        }
                        else
                        {
                            //await DisplayAlert("Code", Encoding.Default.GetString(response), "Okay");
                            UserSession session = JsonConvert.DeserializeObject<UserSession>(Encoding.Default.GetString(response));
                            await DisplayAlert("Success", "Hi " + session.username + "!", "OK");

                            //await Navigation.PushModalAsync(new MapPage(session));
                            Navigation.InsertPageBefore(new MapPage(session), this);
                            await Navigation.PopAsync();
                        }
                    //}
                    //catch(Exception ex)
                    //{
                    //    await DisplayAlert("Warning", "An internet connection is required to play CodeRace.", "OK");
                    //}



                }
            }

        }
    }
}