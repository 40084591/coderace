﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {
        public GamePage(UserSession session)
        {
            InitializeComponent();
        }

        async void GameStartClicked(object sender, EventArgs args)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long starttime = (long)(DateTime.UtcNow - epoch).TotalMilliseconds;
            //TestLabel.Text = starttime.ToString();

            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection
                {
                    ["starttime"] = starttime.ToString()
                };

                var response = client.UploadValues("http://coderace.napier.ac.uk/game.php", postData);
                TestLabel.Text = Encoding.Default.GetString(response);
            }
            
        }

        async void GameResetClicked(object sender, EventArgs args)
        {
            WebClient client = new WebClient();

            var response = client.DownloadString("http://coderace.napier.ac.uk/game.php");

            await DisplayAlert("Reset", response, "OK");
        }
    }
}