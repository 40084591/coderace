﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using coderace;
using System.Collections.Specialized;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
		public MapPage (UserSession session)
		{
			InitializeComponent ();
            
            if(!session.username.Equals("admin"))
            {
                //Check is a game is running. If not, boot the user to the wait screen.
                WebClient client = new WebClient();
                string isgame = client.DownloadString("http://coderace.napier.ac.uk/checkgame.php");
                if (isgame.Equals("0"))
                {
                    Navigation.PushModalAsync(new WaitPage(session));
                }
            }
            
            var locator = CrossGeolocator.Current;
            

            var map = new Map(
                MapSpan.FromCenterAndRadius(
                        new Position(55.9333951, -3.2129722), Distance.FromMiles(0.3)))
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            var unclaim = new Button
            {
                Text = "Unclaim",
                WidthRequest = 960,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            unclaim.Clicked += async (s, e) =>
            {

                WebClient client = new WebClient();
                var response = client.DownloadString("http://coderace.napier.ac.uk/unclaim.php/");
                UpdateLocations(this, session, map, true);
                await DisplayAlert("Unclaimed", response, "OK");
            };

            var addlocation = new Button
            {
                Text = "Add Location",
                WidthRequest = 960,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            addlocation.Clicked += async (s, e) =>
            {
                await Navigation.PushModalAsync(new AddLocation());
            };

            var gamepage = new Button
            {
                Text = "Game Page",
                WidthRequest = 960,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            gamepage.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new GamePage(session));
            };

            var message = new Button
            {
                Text = "Messages",
                WidthRequest = 960,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            message.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new MessagePage(session));
            };

            var scoreboard = new Button
            {
                Text = "Scores",
                WidthRequest = 960,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            scoreboard.Clicked += async (s, e) =>
            {
                //NavigationPage to move to the scoreboard
                await Navigation.PushAsync(new ScoreboardPage(session, false));
       
            };

            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Remove(map);
            stack.Children.Add(map);
            if (session.username.Equals("admin"))
            {
                stack.Children.Add(unclaim);
                stack.Children.Add(addlocation);
                stack.Children.Add(gamepage);
            }
            Content = stack;
            stack.Children.Add(scoreboard);
            stack.Children.Add(message);
            try
            {
                UpdateLocations(this, session, map, true);
            }
            catch(Exception ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }



            if (locator.IsListening)
            {
                //Should probably do something here
            }
            else
            {
                TimeSpan timeforupdates = new TimeSpan(0, 0, 5);
                locator.StartListeningAsync(timeforupdates,40);
            }

            //Update the state of play
            locator.PositionChanged += (sender, e) =>
            {
                try
                {
                    UpdateLocations(this, session, map, true);
                }
                catch(Exception ex)
                {
                    DisplayAlert("Error", "UPDATE FAILED: " + ex.Message, "OK");
                }
            };

        }

        

        //Method for the Help button being clicked - will be identical to the help button in the log in page
        async void HelpClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HelpPage());
        }


        //Method for updating the locations.
        public static void UpdateLocations(MapPage page, UserSession session, Map map, Boolean initial)
        {
            //Check if the game is still running
            //If not, boot to the scoreboard with no back button
            //Check is a game is running. If not, boot the user to the wait screen.
            WebClient client = new WebClient();
            
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long current_time = (long)(DateTime.UtcNow - epoch).TotalMilliseconds;
            string isgame = client.DownloadString("http://coderace.napier.ac.uk/checkgame.php");
            if (long.Parse(isgame) == 0)
            {
                //Do nothing, this is handled earlier
            }
            else if ((current_time >= long.Parse(isgame)) && !(session.username.Equals("admin")))
            {
                try
                {
                    page.Navigation.PushModalAsync(new NavigationPage(new ScoreboardPage(session, true)));
                }
                catch (Exception ex)
                {
                    page.DisplayAlert("Error", "SCOREBOARD PUSH FAILED: " + ex.Message, "OK");
                }

            }
            
            //Remove the already existing pins on the map
            for (int i = 0; i < map.Pins.Count(); i++)
            {
                map.Pins.RemoveAt(i);
            }

            List<ClueObject> clues = new List<ClueObject>();
            List<Pin> pins = new List<Pin>();

            string json = client.DownloadString("http://coderace.napier.ac.uk/clues.php");
            try
            {
                clues = JsonConvert.DeserializeObject<List<ClueObject>>(json);
                //page.DisplayAlert("Succss", "The JSON deserialiser ran", "OK");
            }
            catch (Exception e)
            {
                page.DisplayAlert("Error", "JSON DESERIALISER FAILED: " + e.Message, "OK");
            }

            //Adding clues to the map
            foreach (var clue in clues)
            {
                if (clue.visible)
                {
                    Pin pin = new Pin()
                    {
                        Position = new Position(clue.latitude, clue.longitude),
                        Label = clue.description
                    };

                    pin.Clicked += async (sender, e) =>
                    {
                        if (clue.claimed)
                        {
                            await page.DisplayAlert("Warning!", "Location has already been claimed!", "OK");
                            return;
                        }

                        string usercode = await InputBox(map.Navigation, clue, session);

                        if (usercode.ToLower().Equals(clue.code))
                        {

                            using (client)
                            {
                                NameValueCollection postData = new NameValueCollection
                                {
                                    ["username"] = session.username,
                                    ["location_id"] = clue.id,
                                    ["player_id"] = session.id,
                                    ["team_id"] = session.team_id
                                };

                                var response = client.UploadValues("http://coderace.napier.ac.uk/claim.php", postData);

                                await page.DisplayAlert("Success", Encoding.Default.GetString(response), "OK");
                            }


                            //Update locations after a successful attempt
                            try
                            {
                                UpdateLocations(page, session, map, false);
                            }
                            catch(Exception ex)
                            {
                                await page.DisplayAlert("Error", "NESTED UPDATE FAILED: " + ex.Message, "OK");
                            }
                        }
                        else if (session.username.Equals("admin"))
                        {
                            //Handling admin events
                            await page.DisplayAlert("Admin Update", usercode, "OK");

                            try
                            {
                                UpdateLocations(page, session, map, false);
                            }
                            catch(Exception ex)
                            {
                                await page.DisplayAlert("Error", "ADMIN NESTED UPDATE FAILED: " + ex.Message, "OK");
                            }
                        }
                        else
                        {
                            await page.DisplayAlert("Fail", "Incorrect, try again.", "OK");
                            try
                            {
                                UpdateLocations(page, session, map, false);
                            }
                            catch(Exception ex)
                            {
                                await page.DisplayAlert("Error", "ANSWER FAILED NESTED UPDATE FAILED: " + ex.Message, "OK");
                            }

                        }
                    };
                    
                    //Add the new pins to the map
                    map.Pins.Add(pin);


                }
            }
        }

        //Method for text pop-up - credit to ThomasFlemming at https://forums.xamarin.com/discussion/35838/how-to-do-a-simple-inputbox-dialog
        public static Task<string> InputBox(INavigation navigation, ClueObject clue, UserSession session)
        {
            var tcs = new TaskCompletionSource<string>();
            //Admin controls
            if (session.username.Equals("admin"))
            {
                var title = new Label { Text = "Admin controls", HorizontalOptions = LayoutOptions.Center, FontAttributes = FontAttributes.Bold };
                var cluetext = new Label { Text = "Clue text: " + clue.clue, HorizontalOptions = LayoutOptions.Center };
                var clueanswer = new Label { Text = "Clue answer: " + clue.code, HorizontalOptions = LayoutOptions.Center };
                var updatetext = new Entry { Text = "" };

                WebClient client = new WebClient();

                var adminupdate = new Button
                {
                    Text = "Update Clue",
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8)
                };

                //var adminclue = new Button

                //Handler for the Admin Update button
                adminupdate.Clicked += async (s, e) =>
                {
                    using (client)
                    {
                        NameValueCollection postData = new NameValueCollection
                        {
                            ["is_delete"] = "0",
                            ["new_clue"] = updatetext.Text,
                            ["clue_id"] = clue.id,
                            ["is_clue"] = "1"
                        };

                        var response = client.UploadValues("http://coderace.napier.ac.uk/admin.php", postData);

                        tcs.SetResult(Encoding.Default.GetString(response));
                    }

                    await navigation.PopModalAsync();
                };

                var admindelete = new Button
                {
                    Text = "Delete",
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8)
                };

                //Handler for the Admin Delete button
                admindelete.Clicked += async (s, e) =>
                {
                    using (client)
                    {
                        NameValueCollection postData = new NameValueCollection
                        {
                            ["is_delete"] = "1",
                            ["new_clue"] = "empty",
                            ["clue_id"] = clue.id
                        };

                        var response = client.UploadValues("http://coderace.napier.ac.uk/admin.php", postData);
                        tcs.SetResult(Encoding.Default.GetString(response));
                    }

                    await navigation.PopModalAsync();
                };



                var buttonstack = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal,
                    Children = { admindelete, adminupdate },
                };

                var layout = new StackLayout
                {
                    Padding = new Thickness(0, 40, 0, 0),
                    VerticalOptions = LayoutOptions.StartAndExpand,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    Orientation = StackOrientation.Vertical,
                    Children = { title, cluetext, clueanswer, updatetext, buttonstack },
                };
                // create and show page
                var page = new ContentPage
                {
                    Content = layout
                };
                navigation.PushModalAsync(page);
                updatetext.Focus();
            }
            else
            {
                var title = new Label { Text = clue.clue, HorizontalOptions = LayoutOptions.Center, FontAttributes = FontAttributes.Bold };
                var message = new Label { Text = "Enter your answer:" };
                var input = new Entry { Text = "" };

                var okbutton = new Button
                {
                    Text = "Ok",
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
                };
                okbutton.Clicked += async (s, e) =>
                {
                    // close page
                    var result = input.Text;
                    await navigation.PopModalAsync();
                    // pass result
                    tcs.SetResult(result);
                };

                var cancelbutton = new Button
                {
                    Text = "Cancel",
                    WidthRequest = 100,
                    BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8)
                };
                cancelbutton.Clicked += async (s, e) =>
                {
                    // close page
                    await navigation.PopModalAsync();
                    // pass empty result
                    tcs.SetResult("Incorrect");    //crashes on Android
                };

                var buttonstack = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal,
                    Children = { okbutton, cancelbutton },
                };

                var layout = new StackLayout
                {
                    Padding = new Thickness(0, 40, 0, 0),
                    VerticalOptions = LayoutOptions.StartAndExpand,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    Orientation = StackOrientation.Vertical,
                    Children = { title, message, input, buttonstack },
                };
                // create and show page
                var page = new ContentPage
                {
                    Content = layout
                };
                navigation.PushModalAsync(page);
                // open keyboard
                input.Focus();
            }
            

            // code is waiting here, until result is passed with tcs.SetResult() in btn-Clicked
            // then proc returns the result
            return tcs.Task;
        }
    }
}
