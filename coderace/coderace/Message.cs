﻿using System;
using System.Collections.Generic;
using System.Text;

namespace coderace
{
    class Message
    {
        public string message { get; set; }
        public string username { get; set; }

        //Constructor to be invoked by the JSON deserialiser
        public Message()
        {

        }

    }
}
