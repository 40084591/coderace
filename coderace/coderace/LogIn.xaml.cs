﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LogIn : ContentPage
	{
		public LogIn ()
		{
			InitializeComponent ();
		}

        async void LogInClicked(object sender, EventArgs args)
        {
            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection
                {
                    ["username"] = username.Text,
                    ["password"] = password.Text
                };
                //try
                //{
                    var response = client.UploadValues("http://coderace.napier.ac.uk/login.php", postData);

                    if (Encoding.Default.GetString(response).Equals("Unsuccessful login"))
                    {
                        await DisplayAlert("Whoops!", Encoding.Default.GetString(response), "OK");
                    }
                    else
                    {
                        //await DisplayAlert("Code", Encoding.Default.GetString(response), "Okay");
                        UserSession session = JsonConvert.DeserializeObject<UserSession>(Encoding.Default.GetString(response));
                        await DisplayAlert("Success", "Hi " + session.username + "!", "OK");

                    //await Navigation.PushModalAsync(new MapPage(session));
                    try
                    {
                        Navigation.InsertPageBefore(new MapPage(session), this);
                        await Navigation.PopAsync();
                    }
                    catch(Exception e)
                    {
                        await DisplayAlert("Error", e.Message, "OK");
                    }

                    }
                //}
                //catch(Exception ex)
                //{
                //    await DisplayAlert("Warning", "An internet connection is required to play CodeRace.", "OK");
                //}


            }

        }

        //Method for the register button being clicked.
        async void RegisterClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new RegisterPage());
        }

        //Method for the Help button being clicked - will be identical to the help button in the log in page
        async void HelpClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HelpPage());
        }
    }
}