﻿using System;
using System.Collections.Generic;
using System.Text;

namespace coderace
{
    public class UserSession
    {
        //Properties
        public string username { get; set; }
        public string id { get; set; }
        public string team_id { get; set; }

        //Constructor - will be invoked by JSON Deserialiser
        public UserSession()
        {
        }

    }

}
