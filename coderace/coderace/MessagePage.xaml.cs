﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MessagePage : ContentPage
	{
        //public variables to send to the sent message handler
        public UserSession publicsession;

        public MessagePage(UserSession session)
        {
            InitializeComponent();

            publicsession = session;

            //Set up background worker in order to retrieve messages every 15 seconds or so
            var worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerAsync();

            try
            {
                RetrieveMessages(publicsession, this);
            }
            catch (Exception e)
            {
                DisplayAlert("Error", e.Message, "OK");
            }
        }

        public static void RetrieveMessages(UserSession session, MessagePage page)
        {
            WebClient client = new WebClient();

            List<Message> messagelist = new List<Message>();

            page.messageview.Children.Clear();

            try
            {
                using (client)
                {
                    NameValueCollection postData = new NameValueCollection
                    {
                        ["player_id"] = session.id,
                        ["team_id"] = session.team_id,
                        ["send"] = "0",
                        ["message"] = "none",
                        ["time"] = "none"
                    };

                    var response = client.UploadValues("http://coderace.napier.ac.uk/message.php", postData);

                    messagelist = JsonConvert.DeserializeObject<List<Message>>(Encoding.Default.GetString(response));
                }
            }
            catch (Exception e)
            {
                page.DisplayAlert("Error", e.Message, "OK");
            }

            foreach (var message in messagelist)
            {
                Label user = new Label()
                {
                    Text = message.username,
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.EndAndExpand,
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    FontAttributes = FontAttributes.Italic
                };

                Label content = new Label()
                {
                    Text = message.message,
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.EndAndExpand,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                };

                page.messageview.Children.Add(user);
                page.messageview.Children.Add(content);

                // draws a separator line and space of 5 above and below the separator
                // This is to make it clear to the user that messages are separate
                
                page.messageview.Children.Add(new BoxView() { Color = Color.White, HeightRequest = 5 });
                page.messageview.Children.Add(new BoxView() { Color = Color.Gray, HeightRequest = 1, Opacity = 0.5 });
                page.messageview.Children.Add(new BoxView() { Color = Color.White, HeightRequest = 5 });
            }
        }

        void Entry_Completed(object sender, EventArgs e)
        {
            WebClient client = new WebClient();

            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long current_time = (long)(DateTime.UtcNow - epoch).TotalMilliseconds;

            List<Message> messagelist = new List<Message>();

            try
            {
                using (client)
                {
                    NameValueCollection postData = new NameValueCollection
                    {
                        ["player_id"] = publicsession.id,
                        ["team_id"] = publicsession.team_id,
                        ["send"] = "1",
                        ["message"] = entrytext.Text,
                        ["time"] = current_time.ToString()
                    };
                    var response = client.UploadValues("http://coderace.napier.ac.uk/message.php", postData);
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }

            RetrieveMessages(publicsession, this);
            entrytext.Text = "";
            scrollview.ScrollToAsync(messageview, ScrollToPosition.End, false);
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                //Check for messages every 15 seconds
                Thread.Sleep(15000);
                RetrieveMessages(publicsession, this);
            }
        }
    }
}