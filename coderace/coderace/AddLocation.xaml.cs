﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace coderace
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddLocation : ContentPage
	{
		public AddLocation ()
		{
			InitializeComponent ();
		}

        async void AddButton_Clicked(object sender, EventArgs e)
        {
            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection
                {
                    ["latitude"] = latitude.Text,
                    ["longitude"] = longitude.Text,
                    ["code"] = code.Text,
                    ["clue"] = clue.Text
                };

                var response = client.UploadValues("http://coderace.napier.ac.uk/add.php", postData);

                await DisplayAlert("Add Location", Encoding.Default.GetString(response), "OK");
            }

            await Navigation.PopModalAsync();
        }

        async void CancelButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}