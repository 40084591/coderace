﻿using System;

namespace coderace
{
    public class ClueObject
    {
        //Auto-implemented properties
        public string id { get; set; }
        public string game_Id { get; set; }
        public string type { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string clue { get; set; }
        public Boolean visible { get; set; }
        public string player_id { get; set; }
        public Boolean claimed { get; set; }

        //Constructor - will be invoked by JSON deserialiser
        public ClueObject()
        {
        }
    }

}